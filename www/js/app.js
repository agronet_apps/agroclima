// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('proyect', [
  'ionic',
  'ionic-datepicker',
  'chart.js',
  '$selectBox',
  'ionic.utils',
  'proyect.constantes',
  'proyect.directives',
  'proyect.services',
  'proyect.controllers.perfilCtrl',
  'proyect.controllers.menuOptionCtrl',
  'proyect.controllers.cambioClaveCtrl',
  'proyect.controllers.menuCtrl',
  'proyect.controllers.inicioCtrl',
  'proyect.controllers.loginCtrl',
  'proyect.controllers.reporteClimaCtrl',
  'proyect.controllers.crearPlanRiegoCtrl',
])

.run(function($ionicPlatform , LS) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    cache:false,
    templateUrl: 'templates/home.html'
  })

  //home
  .state('app.home', {
    url: '/home',
	cache: false,
    views: {
      'menuContent': {
        templateUrl: 'templates/inicio.html',
        controller: 'inicioCtrl'
      }
    }
  })
  .state('terminos', {
    url: '/terminos',
    cache: false,
    templateUrl: 'templates/terminos.html',
  })
  .state('politicasPrivacidad', {
    url: '/politicasPrivacidad',
    cache: false,
    templateUrl: 'templates/politicasPrivacidad.html',
  })

  //Init App
  .state('start', {
    url: '/start',
    cache: false,
    templateUrl: 'templates/start.html',
    controller: "menuCtrl"
  })

  //Login
  .state('app.login', {
    url: '/login',
    cache: false,
    views: {
      'menuContent': {
         templateUrl: 'templates/login/login.html',
         controller: 'loginCtrl'
      }
    }
  })

  //Cambio Clave
  .state('app.cambioClave', {
    url: '/cambioClave',
    cache: false,
    views: {
      'menuContent': {
         templateUrl: 'templates/login/cambioClave.html',
         controller: 'cambioClaveCtrl'
      }
    }
  })

  //Perfil
  .state('app.perfil', {
    url: '/perfil',
    cache: false,
    views: {
      'menuContent': {
         templateUrl: 'templates/login/perfil.html',
         controller: 'perfilCtrl'
      }
    }
  })

  //Reeporte clima
  .state('app.reporteClima', {
    url: '/reporteClima',
    // cache: false,
    views: {
      'menuContent': {
         templateUrl: 'templates/reporteClima.html',
         controller: 'reporteClimaCtrl'
      }
    }
  })

  //crear Reeporte clima
  .state('app.crearPlanRiego', {
    url: '/crearPlanRiego',
    cache: false,
    views: {
      'menuContent': {
         templateUrl: 'templates/crearPlanRiego.html',
         controller: 'crearPlanRiegoCtrl'
      }
    }
  })

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('start');
});

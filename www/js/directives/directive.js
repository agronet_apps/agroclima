 angular.module('proyect.directives', [])


.directive('menuRegistre', function () {
  return {
    restrict: 'E',
    templateUrl: 'templates/directives/registre.html'
  };
})

.directive('menuSugesstion', function () {
  return {
    restrict: 'E',
    templateUrl: 'templates/directives/sugesstion.html'
  };
})

.directive('cellView', function () {
  return {
    restrict: 'E',
    templateUrl: 'templates/directives/dataView.html'
  };
})

.directive('profileView', function () {
  return {
    restrict: 'E',
    templateUrl: 'templates/directives/profile.html',
    controller: 'menuOptionCtrl'
  };
})
.factory('FlightDataService', function($q, $timeout ,$localstorage) {

    var searchAirlines = function(searchFilter) {
         
        console.log('Searching airlines for ' + searchFilter);

        var deferred = $q.defer();
        airlines = JSON.parse($localstorage.get('municipios'));
	    var matches = airlines.filter( function(airline) {
	    	if(airline.nombre.toLowerCase().indexOf(searchFilter.toLowerCase()) !== -1 ) return true;
	    })

        $timeout( function(){
        
           deferred.resolve( matches );

        }, 100);

        return deferred.promise;

    };

    return {

        searchAirlines : searchAirlines

    }
})


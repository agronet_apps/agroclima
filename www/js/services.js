/******************************************************************************
 *                                                                            *
 *     Este fichero se optimiza para evitar la redundancia de codigo en el    *
 *    Consumo de servicios web con el fin de menor peso en la aplicación      *
 *                                                                            *
 ******************************************************************************/
var services = angular.module('proyect.services', []);

services.service('usuarioService', function( API, MENSAJES_ERROR, $state, $ionicLoading, $http, $ionicPopup, $q ){

    return  {
      /******************************************************************************
       *                                                                            *
       *      Funcion para todos los servicios que necesiten envio de datos         *
       *                                                                            *
       ******************************************************************************/
      consultasUsuario : function(URL , datosOutput, stateError){
        // console.log("################################ SERVICE ############################");
        // console.log("URL "+URL);
        console.log("Datos "+JSON.stringify(datosOutput));
        // console.log("################################ SERVICE ############################");
        var deferred = $q.defer();
        var promise = deferred.promise;
        $http({
        method:  API.METHOD,
        headers: API.HEADERS,
        timeout : API.TIME_OUT,
        cache : false ,
        url: URL,
        data : datosOutput
        }).success(function (response , status , headers ,config   ) {
          var respuesta = {
            response : '',
            status	: '',
            headers	: '',
          };
          respuesta.response = response;
          respuesta.status	 = status;
          respuesta.headers	 = headers;

            deferred.resolve(respuesta);
            return response;
        }).error(function(response , status , headers ,config ,statusText) {
          $ionicLoading.hide();
          var alertPopup = $ionicPopup.alert({
             title:     MENSAJES_ERROR.TITULO_OK,
             template:  MENSAJES_ERROR.CONEXION_TIME_OUT,
          });
          if(stateError !== '' && stateError !== undefined){
            alertPopup.then(function(){
                $state.go(stateError);
            });

          }
          var respuesta = {
            response : '',
            status	: '',
            headers	: '',
          };
          respuesta.response = response;
          respuesta.status	 = status;
          respuesta.headers	 = headers;

          deferred.reject(respuesta);
          return response;
        });

        promise.success = function(fn) {
            promise.then(fn);
            return promise;
        }
        promise.error = function(fn) {
            promise.then(null, fn);
            return promise;
        }
        return promise;
      },

    /******************************************************************************
     *                                                                            *
     *      Funcion para todos los servicios que no necesiten envio de datos      *
     *                                                                            *
     ******************************************************************************/
    consultasInformacion : function(URL , stateError){
      var deferred = $q.defer();
      var promise = deferred.promise;
      $http({
        method:  API.METHOD,
        headers: API.HEADERS,
        timeout : API.TIME_OUT,
        url: URL,
      }).success(function (response , status , headers ,config ,statusText  ) {
        var respuesta = {
          response : '',
          status	: '',
          headers	: '',
        };
        respuesta.response = response;
        respuesta.status	 = status;
        respuesta.headers	 = headers;

          deferred.resolve(respuesta);
          return response;
      }).error(function(response , status , headers ,config ,statusText) {
          $ionicLoading.hide();
          var alertPopup = $ionicPopup.alert({
             title:     MENSAJES_ERROR.TITULO_OK,
             template:  MENSAJES_ERROR.CONEXION_TIME_OUT,
          });
          var respuesta = {
            response : '',
            status	: '',
            headers	: '',
          };
          respuesta.response = response;
          respuesta.status	 = status;
          respuesta.headers	 = headers;


          console.log("Respuesta "+JSON.stringify(respuesta));
          deferred.reject(respuesta);
          if(stateError !== '' && stateError !== undefined){
            alertPopup.then(function(){
              $state.go(stateError);
            });
          }
          return response;
      });

      promise.success = function(fn) {
          promise.then(fn);
          return promise;
      }
      promise.error = function(fn) {
          promise.then(null, fn);
          return promise;
      }
      return promise;
    },
    /******************************************************************************
     *                                                                            *
     *      Funcion para todos los servicios que necesiten envio de datos         *
     *                                                                            *
     ******************************************************************************/

  }
  });

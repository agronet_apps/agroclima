angular.module('proyect.controllers.menuOptionCtrl', [])
.controller('menuOptionCtrl',  function(
  $scope,
  $state,
  LS,
  $localstorage){

    console.log($localstorage.get(LS.LOGIN));
    $scope.login = $localstorage.get(LS.LOGIN);


    /** Funcion encargada de decidir cada link del menu */
    $scope.goTo = function(tag){

      switch (tag) {
        case 1:
          $state.go('app.login')
          break;
        case 2:
          $state.go('app.cambioClave')
          break;
        case 3:
          $state.go('app.perfil')
          break;
        default:
      }

    }

    /** Funcion encargada de cerrar la sesion */
    $scope.cerrarSession = function () {
      console.log("Cerrando sesion");
      $localstorage.set(LS.LOGIN, false);
      localStorage.removeItem(LS.LOGIN);
      $localstorage.set('token','');
      localStorage.removeItem('token');
      $localstorage.set('usuario','');
      localStorage.removeItem('usuario');
      $scope.login = false;
      $state.go('start');
    }

})

angular.module('proyect.controllers.menuCtrl', ['ngGeolocation'])

.controller('menuCtrl', function($scope, $ionicScrollDelegate, $geolocation ,$ionicHistory,$state , API , usuarioService , $ionicPopup , MENSAJES_INFORMATIVOS  , $ionicLoading , MENSAJES_ERROR){

	localStorage.setItem('LAT',4.700362);
    localStorage.setItem('LON',-74.050154);

// alert('State '+$ionicHistory.currentStateName());
	// if($ionicHistory.currentStateName() === 'app.home'){
	// 	$scope.state = true;
	// }else{
	// 	$scope.state = false;
	// }
	// $geolocation.getLocation({
	// 			timeout: 2000
	// 	 }).then(function(position){
	// 		 alert.log(JSON.stringify(position))

	// 		 localStorage.setItem('LON','1,6');
	// 		 localStorage.setItem('LAT','1,6');
	// 	 }).catch(function(){
	// 		 $ionicLoading.hide();
	// 		 console.log("Error en el geolocation de consulta de posicion");
	// 		 var alertPopup = $ionicPopup.alert({
	// 				title:     MENSAJES_ERROR.TITULO_OK,
	// 				template:  MENSAJES_ERROR.ACTIVACION_GPS,
	// 		 });
	// 	 });

	$ionicLoading.show({
		template: '<ion-spinner icon="lines" class="spinner-calm"></ion-spinner></br> '+MENSAJES_INFORMATIVOS.BUSCANDO_POSICION
	});
// 	// window.navigator.geolocation.getCurrentPosition(function(position) {

// 	navigator.geolocation.getCurrentPosition(function(position) {
// 	          console.log("Position "+position);
// 						localStorage.setItem('LAT',position.coords.latitude);
// 						localStorage.setItem('LON',position.coords.longitude);
// 						// $ionicLoading.hide();
// 						// serviciotmp();
// 	        }, function(error) {
// 							$ionicLoading.hide();
// 							var alertPopup = $ionicPopup.alert({
// 											title:     MENSAJES_ERROR.TITULO_OK,
// 											template:  MENSAJES_ERROR.ACTIVACION_GPS,
// 									 });
//
// });
							// localStorage.setItem('LAT',4.700362);
							// localStorage.setItem('LON',-74.050154);

	            // console.log(JSON.stringify(error));
							var localizacionUsuario = {
									Long:localStorage.getItem('LON'),
									Lat:localStorage.getItem('LAT'),
							};
							$ionicLoading.show({
								template: '<ion-spinner icon="lines" class="spinner-calm"></ion-spinner></br> Buscando estacion'
							});
							usuarioService.consultasUsuario( API.URL_ESTACION, localizacionUsuario, '')
							.success(function(response){
									console.log("Success en el servicio de consulta de estacion status:"+response.status);
									console.log("Success en el servicio de consulta de estacion status:"+JSON.stringify(response.response));
									var dataResponse = response.response.Data;
									$ionicLoading.hide();
									if(response.response.Estado === -1){
									var ciudadActual = {
													nombre : 'Bogota',
													id : '11100000',
													codigo : 11100000,
													key: 11100000
												};
								}else{
										var	ciudadActual = {
															nombre : dataResponse.Nombre,
															id : dataResponse.MunicipioId,
															codigo :  dataResponse.CodCiudadPronostico,
															key: dataResponse.Id
														};
										}
								localStorage.setItem('estacion' , JSON.stringify(ciudadActual));
									}).error(function(response){

									});

				// 			var alertPopup = $ionicPopup.alert({
				// 							title:     MENSAJES_ERROR.TITULO_OK,
				// 							template:  MENSAJES_ERROR.ACTIVACION_GPS,
				// 					 });
	      //   });*/
				 //
				// 	$geolocation.getCurrentPosition({
        //     timeout: 60000
        //  }).then(function(position) {
				// 	 console.log("Position "+position);
				// 	 localStorage.setItem('LAT',position.coords.latitude);
				// 	 localStorage.setItem('LON',position.coords.longitude);
				// 	 $ionicLoading.hide();
				// 	 serviciotmp();
        //  });
					// var posOptions = {timeout: 5000, enableHighAccuracy: false};
					//   $cordovaGeolocation
					//     .getCurrentPosition(posOptions)
					//     .then(function (position) {
					//       // var lat  = position.coords.latitude
					//       // var long = position.coords.longitude
					// 			localStorage.setItem('LAT',position.coords.latitude);
					// 			localStorage.setItem('LON',position.coords.longitude);
					//     }, function(err) {
					//     	console.log(error);
					//     });
	$scope.stateRegistre = true
	$scope.stateSugesstion = false

	$scope.activeBorderColorR = "border-color: #2F6850; background-color: #6abb06; color: white; "
	$scope.activeBorderColorS = "border-color: #59595c; background-color: #f2f2f2; color: #59595c;"

	console.log("cargando ubicacion");

	$scope.actionButtonRegistre = function(){

		$ionicScrollDelegate.scrollTop();
		$scope.stateRegistre = true
		$scope.stateSugesstion = false
		$scope.activeBorderColorS = "border-color: #2F6850; background-color: #f2f2f2; color: #59595c; "
		$scope.activeBorderColorR = "border-color: #59595c; background-color: #6abb06; color: white; "

	}

	$scope.actionButtonSugesstion = function(){

		// if($ionicHistory.currentStateName() === 'app.home'){
		// 	$scope.state = true;
		// }else{
		// 	$scope.state = false;
		// }

		$ionicScrollDelegate.scrollTop();
		$scope.stateRegistre = false
		$scope.stateSugesstion = true
		$scope.activeBorderColorS = "border-color: #2F6850; background-color: #6abb06; color: white; "
		$scope.activeBorderColorR = "border-color: #59595c; background-color: #f2f2f2; color: #59595c; "
	}

	$scope.cleanStack = function(){

		// if($ionicHistory.currentStateName() === 'app.home'){
		// 	$scope.state = true;
		// }else{
		// 	$scope.state = false;
		// }
		$ionicHistory.goBack(-3);
		$scope.userModel.registre = true
		console.log($scope.userModel)
	}

// 	$scope.serviciotmp = function(){
// 		var localizacionUsuario = {
// 				Long:localStorage.getItem('LON'),
// 				Lat:localStorage.getItem('LAT'),
// 		};
// 		$ionicLoading.show({
// 			template: '<ion-spinner icon="lines" class="spinner-calm"></ion-spinner></br> Buscando estacion'
// 		});
// 		usuarioService.consultasUsuario( API.URL_ESTACION, localizacionUsuario, '')
// 		.success(function(response){
// 				console.log("Success en el servicio de consulta de estacion status:"+response.status);
// 				console.log("Success en el servicio de consulta de estacion status:"+JSON.stringify(response.response));
// 				var dataResponse = response.response.Data;
// 				if(response.response.Estado === -1){
// 				var ciudadActual = {
// 								nombre : 'Bogota',
// 								id : '11100000',
// 								codigo : 11100000,
// 								key: 11100000
// 							};
// 			}else{
// 					var	ciudadActual = {
// 										nombre : dataResponse.Nombre,
// 										id : dataResponse.MunicipioId,
// 										codigo :  dataResponse.CodCiudadPronostico,
// 										key: dataResponse.Id
// 									};
// 					}
// 			localStorage.setItem('estacion' , JSON.stringify(ciudadActual));
// 				}).error(function(response){
//
// 				});
// 	};
})

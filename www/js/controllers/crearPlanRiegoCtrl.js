angular.module('proyect.controllers.crearPlanRiegoCtrl', [])

.controller('crearPlanRiegoCtrl',  function( $scope,$ionicHistory,$ionicPopup, $state , MENSAJES_ERROR ,$ionicLoading ,$localstorage,usuarioService,API,$ionicModal){
  $scope.interfaz ={
    municipios : '',
    seleccionado : 'Seleccione Municipio',
    seleccionadoCultivo : 'Seleccione Cultivo',
    dataInsertar : {
      municipio : '',
      fecha : '',
      cultivo : ''
    }
  };

   $scope.interfaz.municipios = JSON.parse(localStorage.getItem('MUNICIPIOS'));
   $ionicModal.fromTemplateUrl('templates/directives/modalMunicipios.html', {
     scope: $scope,
     animation: 'slide-in-up'
   }).then(function(modal) {
     $scope.modal = modal;
   });
  $scope.openModal = function() {
    console.log("AbriendoModal");
   $scope.modal.show();
   };
   $scope.closeModal = function() {
     $scope.modal.hide();
   };
   $ionicModal.fromTemplateUrl('templates/directives/modalCultivos.html', {
     scope: $scope,
     animation: 'slide-in-up'
   }).then(function(modal) {
     $scope.modalCultivos = modal;
   });
   $scope.openModalCultivos = function() {
    console.log("AbriendoModal");
   $scope.modalCultivos.show();
   };
   $scope.closeModalCultivos = function() {
     $scope.modalCultivos.hide();
   };

   $scope.seleccionado= function (municipio){
     $scope.interfaz.seleccionado = municipio.nombre;
     console.log("Municipio "+JSON.stringify(municipio));
     $scope.interfaz.dataInsertar.municipio = municipio.key;
     $scope.modal.hide();
   };
   $scope.seleccionadoCultivos= function (cultivo){
     $scope.interfaz.seleccionadoCultivo = cultivo.CategoriaCultivo;
     console.log("Municipio "+JSON.stringify(cultivo));
     $scope.interfaz.dataInsertar.cultivo = cultivo.Id;
     $scope.modalCultivos.hide();
   };
    $ionicLoading.show({
       template: '<ion-spinner icon="lines" class="spinner-calm"></ion-spinner></br>Cargando informaci&oacute;n... '
     });
     //Consultar categoria cultivo
     $scope.selectCategorias = [ ];

     usuarioService.consultasInformacion(API.URL_CATEGORIAS_CULTIVOS , '')
     .success(function(response){
       $ionicLoading.hide();
       console.log("Success en consultar categoria cultivos Response "+JSON.stringify(response));
      //  console.log("");
      var respuesta = response.response.Data;
        for(var i = 0 ;i < respuesta.length ; i++){
          $scope.selectCategorias[i] ={
                  Id : respuesta[i].IdCultivo,
                  CategoriaCultivo:respuesta[i].NombreCultivo
                }
        }
     }).error(function(response){
       console.log("Error en consultar categoria cultivos Response "+JSON.stringify(response));
     });
  $scope.fecha = "Seleccione una fecha";

  var weekDaysList = ["D", "L", "M", "M", "J", "V", "S"];

  var monthList = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"];

  $scope.datepickerObject = {
        titleLabel: 'Seleccione una fecha',    //Optional
        todayLabel: 'Hoy',    //Optional
        closeLabel: 'Close',    //Optional
        setLabel: 'Ok',    //Optional
        setButtonType : 'button-assertive',  //Optional
        todayButtonType : 'button-assertive',  //Optional
        //closeButtonType : 'button-assertive',  //Optional
        inputDate: new Date(),    //Optional
        mondayFirst: true,    //Optional
        //disabledDates: disabledDates,    //Optional
        weekDaysList: weekDaysList,    //Optional
        monthList: monthList,    //Optional
        templateType: 'popup', //Optional
        modalHeaderColor: 'bar-positive', //Optional
        modalFooterColor: 'bar-positive', //Optional
      //  from: new Date(new Date().setDate(new Date().getDate()-1)),    //Optional
        to: new Date(2018, 8, 25),    //Optional
        callback: function (val) {    //Mandatory
          datePickerCallback(val);
        }
      };


      var datePickerCallback = function (val) {
        if (typeof(val) === 'undefined') {
          console.log('No date selected');
        } else {
          console.log('Selected date is : ', val)

          var year = val.getFullYear();
          var month = val.getMonth() + 1;
          month = (month < 10)?"0"+month:month;
          var day = val.getDate();
          day = (day < 10)?"0"+day:day;

          $scope.fecha = day  + " - " + month + " - " + year;
          $scope.interfaz.dataInsertar.fecha = day+'/'+month+'/'+year;
        }
      };


      $scope.crearPlanRiego = function(){
        var creaplanriego = {
          Token:$localstorage.get('token'),
          IdMunicipio: $scope.interfaz.dataInsertar.municipio,
          IdCultivo:$scope.interfaz.dataInsertar.cultivo,
          FechaCultivo: $scope.interfaz.dataInsertar.fecha,
          IdTipoSuelo: '' };
          console.log("datosPlan "+JSON.stringify(creaplanriego));
          $ionicLoading.show({
             template: '<ion-spinner icon="lines" class="spinner-calm"></ion-spinner></br>Cargando informaci&oacute;n... '
           });
          usuarioService.consultasUsuario(API.URL_CREAR_PLAN,creaplanriego)
          .success(function(response){
            console.log("Succes guardandoPlanRiego "+JSON.stringify(response));
            if(response.response.Estado===1){
                var alertPopup = $ionicPopup.alert({
                 title:     MENSAJES_ERROR.TITULO_OK,
                 template:  'Su plan de riego fue creado satisfactoriamente.'//response.response.Texto
                 });
            }
            else if(response.response.Estado===0){
                var alertPopup = $ionicPopup.alert({
                 title:     MENSAJES_ERROR.TITULO_OK,
                 template:  response.response.Texto
                 }); 
            }
             else{
              alert("Ocurrio un error en el servidor");
             } 
            $ionicLoading.hide();
  					alertPopup.then(function(res) {
  						$state.go('app.home');
  					});
          }).error(function(response){
            console.log("Succes guardandoPlanRiego "+JSON.stringify(response));
          });
      };
})

angular.module('proyect.controllers.cambioClaveCtrl', [])

/** Controlador para el cambio de Clave */
.controller('cambioClaveCtrl',  function(
	$scope,
	$ionicScrollDelegate,
	$ionicHistory,
	$ionicLoading,
	$localstorage,
	LS,
	$state,
	$ionicPopup,
MENSAJES_ERROR,
	usuarioService, API){

	/** Valores por defecto */
  $scope.inputType = 'password';
	$scope.labelVerClave = 'Ver Clave';

  /** Funcion para mostrar y ocultar la clave */
  $scope.hideShowPassword = function(){
    if ($scope.inputType == 'password'){
      $scope.inputType = 'text';
			$scope.labelVerClave = 'Ocultar Clave';
		}
    else{
      $scope.inputType = 'password';
			$scope.labelVerClave = 'Ver Clave';
		}
  };

	/** Funcion para cambiar la clave */
	$scope.cambiarClave = function () {

		$ionicLoading.show({
      template: '<ion-spinner icon="lines" class="spinner-calm"></ion-spinner></br>Cargando ... '
    });
		var data = {
            Nueva:            $scope.claveNueva,
            Confirmacion:     $scope.claveConfirm,
						Actual: 	 $scope.claveAntigua,
						UserId :  $localstorage.get(LS.TOKEN)
        };
				//data: { "UserId":token,"Actual":actual,"Nueva":nueva,"Confirmacion":confirmacion}
		usuarioService.consultasUsuario(API.URL_CAMBIO_CLAVE , data )
		.success(function(response){
			/** Se procesa la respuesta del servido */
			console.log("RESPONSE "+JSON.stringify(response));
			$ionicLoading.hide();
					var alertPopup;
						if(response.response.Estado == 0){
							alertPopup = $ionicPopup.alert({
								 title:     MENSAJES_ERROR.TITULO_ERROR,
								 template:  response.response.Texto
							});
						}else if(response.response.Estado == 1){
							alertPopup = $ionicPopup.alert({
								 title:     MENSAJES_ERROR.TITULO_OK,
								 template:  response.response.Texto
							});

						}
						alertPopup.then(function(res) {
		 				//  $scope.iniciarSesion();
		 				$ionicHistory.nextViewOptions({
		 				  disableAnimate: true,
		 				  disableBack: true
		 				});
		 				 $state.go('app.home');
		 			 });

		}).error(function(response){
			console.log("Error en el servicio de cambio de clave status:"+response.status);
		});
	};

})

angular.module('proyect.controllers.reporteClimaCtrl', [])



.controller('reporteClimaCtrl',  function($scope,$ionicHistory,$ionicScrollDelegate,usuarioService,API , $ionicModal , $ionicLoading){
$scope.ciudad = 'Bogota';
//$scope.mostrarGraficos = false;
  $scope.classTemperaturaMax = 'active';
  $scope.classTemperaturaMin = '';
  $scope.classPrecipita = '';
  $scope.classEvapo = '';

  //  $scope.interfaz = {municipios : JSON.stringify(localStorage.getItem('MUNICIPIOS'))};

  $scope.municipios = JSON.parse(localStorage.getItem('MUNICIPIOS'));
  // console.log(" SUCCESS "+JSON);
  $scope.consulta = {
    municipios : 'Seleccione municipio',
    fechaActual : 'Seleccione fecha'
  };
  /**********************************
  *       Template modal de filtro de las ciudades
  **********************************/
    $ionicModal.fromTemplateUrl('templates/directives/modalMunicipios_2.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal_2 = modal;
    });
   $scope.openModal = function() {
    $scope.modal_2.show();
    };
    $scope.closeModal = function() {
      $scope.modal_2.hide();
    };
    $scope.dataTemperaturaMax  = [];
    $scope.dataTemperatura  = [];
    $scope.dataPrecipita = [];
    $scope.dataEvapo = [];
    $ionicModal.fromTemplateUrl('templates/directives/modalAnos0.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal_1 = modal;
    });
   $scope.openModal_1 = function() {
    $scope.modal_1.show();
    };
    $scope.closeModal_1 = function() {
      $scope.modal_1.hide();
    };

  $scope.mostrarTemperaturasMax = true;
  $scope.mostrarTemperaturas = false;
  $scope.mostrarPrecipita = false;
  $scope.mostrarEvapo = false;

  $scope.promedios = {
      temperaturaMaxima : {
        max : 0 ,
        media : 0 ,
        actual : 0,
      },
      temperaturaMinima : {
        max : 0,
        media : 0 ,
        actual : 0,
      },
      precipitacion : {
        max : 0 ,
        media : 0 ,
        actual : 0,
      },
      Evapotranspiracion : {
        max : 0 ,
        media : 0 ,
        actual : 0,
      },
  };

  yearDataActu = new Date().getYear();
  if(yearDataActu < 1900 ){
    var data = {
        year:( (1900 + new Date().getYear()) -1 ),
        IdMunicipio:'1698'
    };
  }else{
    var data = {
        year:( (new Date().getYear()) - 1),
        IdMunicipio: '1698'
    };
  }
  consultarData(data);
  function consultarData (data){

    $scope.promedios.temperaturaMaxima.max  = 0 ;
    $scope.promedios.temperaturaMaxima.media  = 0 ;
    $scope.promedios.temperaturaMaxima.actual = 0 ;
    $scope.promedios.temperaturaMinima.max = 0 ;
    $scope.promedios.temperaturaMinima.media  = 0 ;
    $scope.promedios.temperaturaMinima.actual = 0 ;
    $scope.promedios.precipitacion.max  = 0 ;
    $scope.promedios.precipitacion.media  = 0 ;
    $scope.promedios.precipitacion.actual = 0 ;
    $scope.promedios.Evapotranspiracion.max  = 0 ;
    $scope.promedios.Evapotranspiracion.media  = 0 ;
    $scope.promedios.Evapotranspiracion.actual = 0 ;

    $ionicLoading.show({
      template: '<ion-spinner icon="lines" class="spinner-calm"></ion-spinner></br>Consultando información ... '
    });
   usuarioService.consultasUsuario( API.URL_CLIMA, data, '')
   .success(function(response){
     $ionicLoading.hide();
     //console.log("Success en la consulta del clima "+JSON.stringify(response.response));
     dataRespuesta = response.response.Data;
    //  console.log("Success en la consulta del clima "+JSON.stringify(dataRespuesta));

     temperaturaMaximaActual = [];
     temperaturaMaximaA = [];
     temperaturaMaximaP = [];

     temperaturaMinimaActual = [];
     temperaturaMinimaA = [];
     temperaturaMinimaP = [];

     precipitacionActual = [];
     precipitacionA = [];
     precipitacionP = [];

     evapotranspiracionActual = [];
     evapotranspiracionA = [];
     evapotranspiracionP = [];
     var contA = 0;
     var contB = 0;
     var contC = 0;
     var anioActual = ( 1900 + (new Date()).getYear());
    //  console.log("$$$$$$$$$$$$$$$ "+anioActual);
     for(var i = 0 ; i < dataRespuesta.length ; i++){
       //console.log("Mes "+dataRespuesta[i].Mes);
        if(dataRespuesta[i].Agno === 1900){
             temperaturaMaximaA [contA]  = parseFloat(dataRespuesta[i].TemperaturaMaxima || 0).toFixed(2);
             evapotranspiracionA [contA] = parseFloat(dataRespuesta[i].Evapotranspiracion || 0).toFixed(2);
             temperaturaMinimaA [contA] = parseFloat(dataRespuesta[i].TemperaturaMinima || 0).toFixed(2);
             precipitacionA [contA] = parseFloat(dataRespuesta[i].Precipitacion || 0).toFixed(2);


             if(isNaN(temperaturaMaximaA [contA])){
               temperaturaMaximaA [contA] = 0 ;
             }
             if(isNaN(evapotranspiracionA [contA])){
               evapotranspiracionA [contA] = 0 ;
             }
             if(isNaN(temperaturaMinimaA [contA])){
               temperaturaMinimaA [contA] = 0 ;
             }
             if(isNaN(precipitacionA [contA])){
               precipitacionA [contA] = 0 ;
             }
             $scope.promedios.temperaturaMaxima.media = parseFloat($scope.promedios.temperaturaMaxima.media) + parseFloat( temperaturaMaximaA [contA]);
             $scope.promedios.temperaturaMinima.media = parseFloat($scope.promedios.temperaturaMinima.media) + parseFloat( temperaturaMinimaA [contA]);
             $scope.promedios.precipitacion.media = parseFloat($scope.promedios.precipitacion.media) +  parseFloat(precipitacionA [contA]);
             $scope.promedios.Evapotranspiracion.media = parseFloat($scope.promedios.Evapotranspiracion.media) + parseFloat(evapotranspiracionA [contA] );
             console.log("$scope.promedios.Evapotranspiracion.media "+$scope.promedios.Evapotranspiracion.media);
            //  console.log("$scope.promedios "+JSON.stringify($scope.promedios) );
             contA = contA + 1 ;
        }else if(dataRespuesta[i].Agno === anioActual ){
            // console.log(JSON.stringify(dataRespuesta[i]));
            $scope.promedios.temperaturaMaxima.actual =parseFloat($scope.promedios.temperaturaMaxima.actual || 0 ) + parseFloat(dataRespuesta[i].TemperaturaMaxima || 0 );
            $scope.promedios.temperaturaMinima.actual =parseFloat($scope.promedios.temperaturaMinima.actual || 0 ) + parseFloat(dataRespuesta[i].TemperaturaMinima || 0);
            $scope.promedios.precipitacion.actual = parseFloat($scope.promedios.precipitacion.actual || 0 ) + parseFloat(dataRespuesta[i].Precipitacion || 0);
            $scope.promedios.Evapotranspiracion.actual =parseFloat($scope.promedios.Evapotranspiracion.actual || 0 ) +  parseFloat(dataRespuesta[i].Evapotranspiracion || 0);
            temperaturaMaximaActual  [contC] = parseFloat(dataRespuesta[i].TemperaturaMaxima || 0);
            temperaturaMinimaActual  [contC] = parseFloat(dataRespuesta[i].TemperaturaMinima || 0);
            precipitacionActual  [contC] = parseFloat(dataRespuesta[i].Precipitacion || 0);
            evapotranspiracionActual  [contC] = parseFloat(dataRespuesta[i].Evapotranspiracion || 0);
            contC = contC + 1 ;
         }else{
           temperaturaMaximaP [contB] = parseFloat(dataRespuesta[i].TemperaturaMaxima) || 0;
           temperaturaMinimaP [contB] = parseFloat(dataRespuesta[i].TemperaturaMinima) || 0;

           evapotranspiracionP [contB] = parseFloat(dataRespuesta[i].Evapotranspiracion) || 0;
           precipitacionP [contB] = parseFloat(dataRespuesta[i].Precipitacion ) || 0;
           if(isNaN(temperaturaMaximaP [contB])){
             temperaturaMaximaP [contB] = 0 ;
           }
           if(isNaN(temperaturaMinimaP [contB])){
             temperaturaMinimaP [contB] = 0 ;
           }
           if(isNaN(evapotranspiracionP [contB])){
             evapotranspiracionP [contB] = 0 ;
           }
           if(isNaN(precipitacionP [contB])){
             precipitacionP [contB] = 0 ;
           }
           console.log("$scope.promedios.Evapotranspiracion.max "+$scope.promedios.Evapotranspiracion.max);
           $scope.promedios.temperaturaMaxima.max = parseFloat( ""+$scope.promedios.temperaturaMaxima.max ) + parseFloat(temperaturaMaximaP [contB]);
           $scope.promedios.temperaturaMinima.max = parseFloat($scope.promedios.temperaturaMinima.max ) + parseFloat(temperaturaMinimaP [contB]);
           $scope.promedios.precipitacion.max = parseFloat($scope.promedios.precipitacion.max ) + parseFloat(precipitacionP [contB]);
           $scope.promedios.Evapotranspiracion.max = parseFloat($scope.promedios.Evapotranspiracion.max ) + parseFloat(evapotranspiracionP [contB] );
           contB = contB + 1;
       }
     }
    //  $scope.$apply(function(){
       $scope.dataTemperaturaMax  = [];
       $scope.dataTemperaturaMax = [temperaturaMaximaA , temperaturaMaximaP  , temperaturaMaximaActual];

       $scope.dataTemperatura  = [];
       $scope.dataTemperatura = [temperaturaMinimaA , temperaturaMinimaP  , temperaturaMinimaActual ];

       $scope.dataPrecipita = [];
       $scope.dataPrecipita = [precipitacionA ,precipitacionP , precipitacionActual];

       $scope.dataEvapo = [];
       $scope.dataEvapo = [evapotranspiracionA , evapotranspiracionP , evapotranspiracionActual];
       //$scope.mostrarGraficos = true;
    //  });
// console.log(JSON.stringify($scope.promedios));
     $scope.promedios.temperaturaMaxima.max = parseFloat(($scope.promedios.temperaturaMaxima.max)/ 12).toFixed(2);
    $scope.promedios.temperaturaMinima.max = parseFloat(($scope.promedios.temperaturaMinima.max)/ 12).toFixed(2);
    $scope.promedios.precipitacion.max = parseFloat(($scope.promedios.precipitacion.max)).toFixed(2);
    console.log("$scope.promedios.Evapotranspiracion.max " + $scope.promedios.Evapotranspiracion.max);
    $scope.promedios.Evapotranspiracion.max =(parseFloat( $scope.promedios.Evapotranspiracion.max )/ 12).toFixed(2);
    $scope.promedios.temperaturaMaxima.media = (parseFloat($scope.promedios.temperaturaMaxima.media) / 12).toFixed(2);
    $scope.promedios.temperaturaMinima.media = (parseFloat($scope.promedios.temperaturaMinima.media) / 12).toFixed(2);
    $scope.promedios.precipitacion.media = (parseFloat($scope.promedios.precipitacion.media) ).toFixed(2);
    console.log("$scope.promedios.Evapotranspiracion.media " + $scope.promedios.Evapotranspiracion.media);
    $scope.promedios.Evapotranspiracion.media = (parseFloat($scope.promedios.Evapotranspiracion.media) / 12).toFixed(2);
    $scope.promedios.temperaturaMaxima.actual = ($scope.promedios.temperaturaMaxima.actual / temperaturaMaximaActual.length).toFixed(2);
    console.log(temperaturaMaximaActual.length);

    $scope.promedios.temperaturaMinima.actual = ($scope.promedios.temperaturaMinima.actual / temperaturaMinimaActual.length).toFixed(2);
    console.log(temperaturaMinimaActual.length);
    $scope.promedios.precipitacion.actual = ($scope.promedios.precipitacion.actual).toFixed(2);
    $scope.promedios.Evapotranspiracion.actual = ($scope.promedios.Evapotranspiracion.actual / evapotranspiracionActual.length).toFixed(2);
// console.log(JSON.stringify($scope.promedios));
     //console.log("dataEvapo "+$scope.dataEvapo);
   }).error(function(response){
     $ionicLoading.hide();
     console.log("Error en la consulta del clima status:"+response.status);
   });
 };
// console.log(JSON.stringify(data));
  //consumno de webservices del pronostico
  var fecha =new Date();

  $scope.anio =( 1900 + fecha.getYear() );

  $scope.labels = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"];
  $scope.series1 = ['Media Climatica', 'Media Año Anterior', 'Media Actual' ];
  $scope.series2 = ['Media Climatica', 'Media Año Anterior', 'Media Actual' ];
  $scope.series3 = ['Media Climatica', 'Media Año Anterior', 'Media Actual' ];
  $scope.series4 = ['Media Climatica', 'Media Año Anterior', 'Media Actual' ];
  $scope.colores1 = ['#f09e29', '#6ab13e', '#57acef'];
  $scope.colores2 = ['#f09e29', '#6ab13e', '#57acef'];
  $scope.colores3 = ['#f09e29', '#6ab13e', '#57acef'];
  $scope.colores4 = ['#f09e29', '#6ab13e', '#57acef'];
  // $scope.dataTemperatura = [
  //                           [55,   40, 40, 10,  56, 80,  40, 56, 46, 34,40,  56],
  //                           [55,   40, 40, 10,   56, 55,   40, 40, 10, 70, 50,  40],
  //
  //                           [28, 48,  40, 19,   86, 27,   60, 45, 69, 67, 78,   67]
  // ];
//   $scope.dataPrecipita = [
// [28, 48,  40, 19,   86, 27,   60, 45, 69, 67, 78,   67],
//                           [55,   40, 40, 10,   56, 55,   40, 40, 10, 70, 50,  40],
//                           [55,   40, 40, 10,  56, 80,  40, 56, 46, 34,40,  56],
//
//   ];
  // $scope.dataEvapo = [
  //
  //     [0.5,   0.0, 0.0, 0.0,  0.6, 0.0,  0.0, 0.6, 0.6, 0.4,0.0,  0.6],
  //     [0.5,   0.0, 0.0, 0.0,  0.6, 0.0,  0.0, 0.6, 0.6, 0.4,0.0,  0.6],
  //     [0.5,   0.0, 0.0, 0.0,  0.6, 0.0,  0.0, 0.6, 0.6, 0.4,0.0,  0.6]
  //     ///[55,   40, 40, 10,  56, 80,  40, 56, 46, 34,40,  56]
  // ];
  $scope.irTemperaturasM = function(){
    $scope.classTemperaturaMax = 'active';
    $scope.classTemperaturaMin = '';
    $scope.classPrecipita = '';
    $scope.classEvapo = '';

    $scope.mostrarTemperaturasMax = true;
    $scope.mostrarTemperaturas = false;
    $scope.mostrarPrecipita = false;
    $scope.mostrarEvapo = false;
     
  };
  $scope.irTemperaturas = function () {
    $scope.classTemperaturaMax = '';
    $scope.classTemperaturaMin = 'active';
    $scope.classPrecipita = '';
    $scope.classEvapo = '';

    $scope.mostrarTemperaturasMax = false;
    $scope.mostrarTemperaturas = true;
    $scope.mostrarPrecipita = false;
    $scope.mostrarEvapo = false;
  };
  $scope.irPrecipita = function () {
    $scope.classTemperaturaMax = '';
    $scope.classTemperaturaMin = '';
    $scope.classPrecipita = 'active';
    $scope.classEvapo = '';

    $scope.mostrarTemperaturasMax = false;
    $scope.mostrarTemperaturas = false;
    $scope.mostrarPrecipita = true;
    $scope.mostrarEvapo = false;
  };
  $scope.irEvapo = function () {
    $scope.classTemperaturaMax = '';
    $scope.classTemperaturaMin = '';
    $scope.classPrecipita = '';
    $scope.classEvapo = 'active';

    $scope.mostrarTemperaturasMax = false;
    $scope.mostrarTemperaturas = false;
    $scope.mostrarPrecipita = false;
    $scope.mostrarEvapo = true;
    console.log(JSON.stringify($scope.dataEvapo));




  };

  $scope.seleccionado = function(municipio){
    $scope.valorMunicipio = municipio.key;
    $scope.consulta.municipios = municipio.nombre;
    $scope.modal_2.hide();
      $scope.buscar();
  };


  $scope.buscar = function(){
    yearDataActu = new Date().getYear();
    if(yearDataActu < 1900 ){
      var data = {
          year:( (1900 + new Date().getYear()) -1 ),
          IdMunicipio: $scope.valorMunicipio
      };
    }else{
      var data = {
          year:( (new Date().getYear())  -1),
          IdMunicipio: $scope.valorMunicipio
      };
    }
//$scope.mostrarGraficos = false;
    $scope.ciudad = ($scope.consulta.municipios).split(",")[0];
    // $scope.$apply(function(){
    $scope.promedios.temperaturaMaxima.max  = 0 ;
    $scope.promedios.temperaturaMaxima.media  = 0 ;
    $scope.promedios.temperaturaMaxima.actual = 0 ;
    $scope.promedios.temperaturaMinima.max = 0 ;
    $scope.promedios.temperaturaMinima.media  = 0 ;
    $scope.promedios.temperaturaMinima.actual = 0 ;
    $scope.promedios.precipitacion.max  = 0 ;
    $scope.promedios.precipitacion.media  = 0 ;
    $scope.promedios.precipitacion.actual = 0 ;
    $scope.promedios.Evapotranspiracion.max  = 0 ;
    $scope.promedios.Evapotranspiracion.media  = 0 ;
    $scope.promedios.Evapotranspiracion.actual = 0 ;

    $ionicLoading.show({
      template: '<ion-spinner icon="lines" class="spinner-calm"></ion-spinner></br>Consultando información ... '
    });
   usuarioService.consultasUsuario( API.URL_CLIMA, data, '')
   .success(function(response){
     $ionicLoading.hide();
     //console.log("Success en la consulta del clima "+JSON.stringify(response.response));
     var dataRespuesta = '';
     dataRespuesta = response.response.Data;
    //  console.log("Success en la consulta del clima "+JSON.stringify(response.response));

     temperaturaMaximaA = [];
     temperaturaMaximaP = [];

     temperaturaMinimaA = [];
     temperaturaMinimaP = [];

     precipitacionA = [];
     precipitacionP = [];

     evapotranspiracionA = [];
     evapotranspiracionP = [];
     var contA = 0;
     var contB = 0;
     var contC = 0;
     var anioActual = ( 1900 + (new Date()).getYear());
    //  console.log("$$$$$$$$$$$$$$$ "+anioActual);
    var promedioAnioAnterior=0;
    var promedioMinimaAnioAnterior=0;
    for(var i = 0 ; i < dataRespuesta.length ; i++){
      //console.log("Mes "+dataRespuesta[i].Mes);
       if(dataRespuesta[i].Agno === 1900){
            temperaturaMaximaA [contA]  = parseFloat(dataRespuesta[i].TemperaturaMaxima || 0).toFixed(2);
            evapotranspiracionA [contA] = parseFloat(dataRespuesta[i].Evapotranspiracion || 0).toFixed(2);
            temperaturaMinimaA [contA] = parseFloat(dataRespuesta[i].TemperaturaMinima || 0).toFixed(2);
            precipitacionA [contA] = parseFloat(dataRespuesta[i].Precipitacion || 0).toFixed(2);


            if(isNaN(temperaturaMaximaA [contA])){
              temperaturaMaximaA [contA] = 0 ;
            }
            if(isNaN(evapotranspiracionA [contA])){
              evapotranspiracionA [contA] = 0 ;
            }
            if(isNaN(temperaturaMinimaA [contA])){
              temperaturaMinimaA [contA] = 0 ;
            }
            if(isNaN(precipitacionA [contA])){
              precipitacionA [contA] = 0 ;
            }
            $scope.promedios.temperaturaMaxima.media = parseFloat($scope.promedios.temperaturaMaxima.media) + parseFloat( temperaturaMaximaA [contA]);
            $scope.promedios.temperaturaMinima.media = parseFloat($scope.promedios.temperaturaMinima.media) + parseFloat( temperaturaMinimaA [contA]);
            $scope.promedios.precipitacion.media = parseFloat($scope.promedios.precipitacion.media) + parseFloat( precipitacionA[contA] );
            $scope.promedios.Evapotranspiracion.media = parseFloat($scope.promedios.Evapotranspiracion.media) + parseFloat( evapotranspiracionA[contA]);
//console.log("->$scope.promedios.Evapotranspiracion.media evapotranspiracionA[contA] "+evapotranspiracionA[contA]);
           //  console.log("$scope.promedios "+JSON.stringify($scope.promedios) );
            contA = contA + 1 ;
       }else if(dataRespuesta[i].Agno === anioActual ){
           // console.log(JSON.stringify(dataRespuesta[i]));
           //$scope.promedios.temperaturaMaxima.actual =parseFloat(dataRespuesta[i].TemperaturaMaxima || 0 ).toFixed(2);
           
          $scope.promedios.temperaturaMaxima.actual =parseFloat($scope.promedios.temperaturaMaxima.actual || 0 ) + parseFloat(dataRespuesta[i].TemperaturaMaxima || 0 );
          $scope.promedios.temperaturaMinima.actual =parseFloat($scope.promedios.temperaturaMinima.actual || 0 ) + parseFloat(dataRespuesta[i].TemperaturaMinima || 0);
          

             console.log("!!!!!"+dataRespuesta[i].Evapotranspiracion);
           //$scope.promedios.temperaturaMinima.actual = parseFloat(dataRespuesta[i].TemperaturaMinima || 0).toFixed(2);
           $scope.promedios.precipitacion.actual = parseFloat(dataRespuesta[i].Precipitacion || 0).toFixed(2);
           if(dataRespuesta[i].Evapotranspiracion!=0){
             $scope.promedios.Evapotranspiracion.actual =  parseFloat($scope.promedios.Evapotranspiracion.actual || 0 ) +parseFloat(dataRespuesta[i].Evapotranspiracion || 0);
             evapotranspiracionActual  [contC] =           parseFloat($scope.promedios.Evapotranspiracion.actual || 0 )+ parseFloat(dataRespuesta[i].Evapotranspiracion || 0);
           }
           temperaturaMaximaActual  [contC] = parseFloat(dataRespuesta[i].TemperaturaMaxima || 0).toFixed(2);
           temperaturaMinimaActual  [contC] = parseFloat(dataRespuesta[i].TemperaturaMinima || 0).toFixed(2);
           precipitacionActual  [contC] = parseFloat(dataRespuesta[i].Precipitacion || 0).toFixed(2);
           
           contC = contC + 1 ;
           console.log("!!!!!"+contC);
           
        }else{
          temperaturaMaximaP [contB] = parseFloat(dataRespuesta[i].TemperaturaMaxima) || 0;
          temperaturaMinimaP [contB] = parseFloat(dataRespuesta[i].TemperaturaMinima) || 0;

         
          evapotranspiracionP [contB] = parseFloat(dataRespuesta[i].Evapotranspiracion) || 0;
          precipitacionP [contB] = parseFloat(dataRespuesta[i].Precipitacion ) || 0;
          if(isNaN(temperaturaMaximaP [contB])){
            temperaturaMaximaP [contB] = 0 ;
          }
          else if (temperaturaMaximaP [contB]==0){
                 promedioAnioAnterior++;
          }
          if(isNaN(temperaturaMinimaP [contB])){
            temperaturaMinimaP [contB] = 0 ;
          }
          else if(temperaturaMinimaP[contB] ==0){
             promedioMinimaAnioAnterior++;
          }
          if(isNaN(evapotranspiracionP [contB])){
            evapotranspiracionP [contB] = 0 ;
          }
          if(isNaN(precipitacionP [contB])){
            precipitacionP [contB] = 0 ;
          }
          $scope.promedios.temperaturaMaxima.max = parseFloat( ""+$scope.promedios.temperaturaMaxima.max ) + parseFloat(temperaturaMaximaP [contB]);
          $scope.promedios.temperaturaMinima.max = parseFloat($scope.promedios.temperaturaMinima.max ) + parseFloat(temperaturaMinimaP [contB]);
          $scope.promedios.precipitacion.max = parseFloat($scope.promedios.precipitacion.max ) + parseFloat(precipitacionP [contB] );
          $scope.promedios.Evapotranspiracion.max = parseFloat($scope.promedios.Evapotranspiracion.max ) + parseFloat(  evapotranspiracionP[contB]);
          contB = contB + 1;
      }
    }
    

    //  $scope.$apply(function(){
  
    //$scope.mostrarGraficos = true;
    //  });
    // //$scope.mostrarGraficos = true;
 console.log(JSON.stringify($scope.dataTemperaturaMax));
 console.log(JSON.stringify($scope.dataTemperatura));
 console.log(JSON.stringify($scope.dataPrecipita));
 console.log(JSON.stringify($scope.dataEvapo));

    $scope.promedios.temperaturaMaxima.actual = ($scope.promedios.temperaturaMaxima.actual / temperaturaMaximaActual.length).toFixed(2);
              if($scope.promedios.temperaturaMaxima.actual ==0){$scope.promedios.temperaturaMaxima.actual ="SD"}
                console.log(temperaturaMaximaActual.length);

    $scope.promedios.temperaturaMinima.actual = ($scope.promedios.temperaturaMinima.actual / contC).toFixed(2);
                if($scope.promedios.temperaturaMinima.actual ==0){$scope.promedios.temperaturaMinima.actual ="SD"}
    
    $scope.promedios.Evapotranspiracion.actual = ($scope.promedios.Evapotranspiracion.actual / contC).toFixed(2);


    $scope.promedios.temperaturaMaxima.max = ($scope.promedios.temperaturaMaxima.max/ (12-promedioAnioAnterior)).toFixed(2);
    $scope.promedios.temperaturaMinima.max = ($scope.promedios.temperaturaMinima.max/ (12-promedioMinimaAnioAnterior)).toFixed(2);
    $scope.promedios.precipitacion.max = ($scope.promedios.precipitacion.max).toFixed(2);
    $scope.promedios.Evapotranspiracion.max =( $scope.promedios.Evapotranspiracion.max/ 12).toFixed(2);
    $scope.promedios.temperaturaMaxima.media = ($scope.promedios.temperaturaMaxima.media / 12).toFixed(2);
    $scope.promedios.temperaturaMinima.media = ($scope.promedios.temperaturaMinima.media / 12).toFixed(2);
    $scope.promedios.precipitacion.media = ($scope.promedios.precipitacion.media ).toFixed(2);
    $scope.promedios.Evapotranspiracion.media = ($scope.promedios.Evapotranspiracion.media / 12).toFixed(2);
// console.log(JSON.stringify($scope.promedios));
     //console.log("dataEvapo "+$scope.dataEvapo);
   



    $scope.dataTemperaturaMax  = [];
    $scope.dataTemperaturaMax = [temperaturaMaximaA , temperaturaMaximaP  , temperaturaMaximaActual];


    $scope.dataTemperatura  = [];
    $scope.dataTemperatura = [temperaturaMinimaA , temperaturaMinimaP  , temperaturaMinimaActual ];

    $scope.dataPrecipita = [];
    $scope.dataPrecipita = [precipitacionA ,precipitacionP , precipitacionActual];

    $scope.dataEvapo = [];
    $scope.dataEvapo = [evapotranspiracionA , evapotranspiracionP , evapotranspiracionActual];
 

  

   }).error(function(response){
     $ionicLoading.hide();
     console.log("Error en la consulta del clima status:"+response.status);
   });

     $scope.labels = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"];
     $scope.series1 = ['Media Climatica', 'Media Año Anterior', 'Media Actual' ];
     $scope.series2 = ['Media Climatica', 'Media Año Anterior', 'Media Actual' ];
     $scope.series3 = ['Media Climatica', 'Media Año Anterior', 'Media Actual' ];
     $scope.series4 = ['Media Climatica', 'Media Año Anterior', 'Media Actual' ];
     $scope.colores1 = ['#f09e29', '#6ab13e', '#57acef'];
     $scope.colores2 = ['#f09e29', '#6ab13e', '#57acef'];
     $scope.colores3 = ['#f09e29', '#6ab13e', '#57acef'];
     $scope.colores4 = ['#f09e29', '#6ab13e', '#57acef'];

    // });
  };
})
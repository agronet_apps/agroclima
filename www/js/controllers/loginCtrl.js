angular.module('proyect.controllers.loginCtrl', [])

/** Controlador para el login */
.controller('loginCtrl',  function(
	LS,
	$scope,
	$ionicScrollDelegate,
	$ionicHistory,
	$ionicLoading,
	$localstorage,
	API,
	$ionicPopup,
	MENSAJES_ERROR,
	$state,
	usuarioService){

	$scope.existUser = true;

	$scope.textButton = "Crear una cuenta"

	$scope.existUserAction = function(){
		$ionicScrollDelegate.scrollTop();
		$scope.existUser = false;
		$scope.textButton =  "Ingresar"
	}

	$scope.goToHome= function() {
		$ionicHistory.goBack(-2);
	}

	/** Valores por defecto */
  $scope.inputType = 'password';
	$scope.labelVerClave = 'Ver Clave';

  /** Funcion para mostrar y ocultar la clave */
  $scope.hideShowPassword = function(){
    if ($scope.inputType == 'password'){
      $scope.inputType = 'text';
			$scope.labelVerClave = 'Ocultar Clave';
		}
    else{
      $scope.inputType = 'password';
			$scope.labelVerClave = 'Ver Clave';
		}
  };

	/** Funcion para registrar a un usuario */
	$scope.registrarUsuario = function () {

		$ionicLoading.show({
      template: '<ion-spinner icon="lines" class="spinner-calm"></ion-spinner></br>Registrando Sesi&oacuten ... '
    });
		var dataUsuario ={ Correo:$scope.correo,
											Clave: $scope.clave,
                  		AppId :     "0",
                  		TokenPush:  "q239890afa8904"
											};
		console.log("dataUsuario registro usuario status: "+JSON.stringify(dataUsuario));
		usuarioService.consultasUsuario(API.URL_REGISTRO_USUARIO, dataUsuario)
		.success(function(response){
			$ionicLoading.hide();
			console.log("Success registro usuario status: "+response.status);
			var data = response.response.Data;
			/************************************
			 * Se controla la informacion que llega del servidor
			 ***********************************/
				if(response.response.Estado == 0){
					var alertPopup = $ionicPopup.alert({
						 title:     MENSAJES_ERROR.TITULO_ERROR,
						 template:  response.response.Texto
					});
					alertPopup.then(function(res) {
						$state.go('app.home');
					});
				}else if(response.response.Estado == 1){
					$localstorage.set('usuario', JSON.stringify({usuario:$scope.correo,clave:$scope.clave}));
					var alertPopup = $ionicPopup.alert({
						 title:     MENSAJES_ERROR.TITULO_OK,
						 template:  response.response.Texto
					});

					alertPopup.then(function(res) {
						$scope.iniciarSesion();
					});

					/** Se guarda el token en el localStorage */
					$localstorage.set(LS.TOKEN, response.response.Token);

					/** Se activa la sesion localStorage */
					$localstorage.set(LS.LOGIN, true);
				}
			}).error(function(response){
				console.log("Error registrando usuario status: "+response.status);
			});
	};

	/** Funcion para que un usuario inicie sesion */
	$scope.iniciarSesion = function () {

		$ionicLoading.show({
      template: '<ion-spinner icon="lines" class="spinner-calm"></ion-spinner></br>Iniciando Sesi&oacuten ... '
    });
		var dataUsuario ={ Email:$scope.correo,
											Password: $scope.clave,
											AppId :       "0",
                  		TokenPush:    "q239890afa8904"
											};
		usuarioService.consultasUsuario(API.URL_INICIAR_SESION, dataUsuario)
		.success(function(response){
			$ionicLoading.hide();
			console.log("Success login usuario status: "+response.status);
			//console.log("Success login usuario data : "+JSON.stringify(response));
			var data = response.response.Data;
			/************************************
			 * Se controla la informacion que llega del servidor
			 ***********************************/
				if(response.response.Estado == 0){
					var alertPopup = $ionicPopup.alert({
						 title:     MENSAJES_ERROR.TITULO_ERROR,
						 template:  response.response.Texto
					});
					alertPopup.then(function(res) {
						$state.go('app.home');
					});
				}else if(response.response.Estado == 1){
					$localstorage.set('usuario', JSON.stringify({usuario:$scope.correo,clave:$scope.clave}));
					var alertPopup = $ionicPopup.alert({
						 title:     MENSAJES_ERROR.TITULO_OK,
						 template:  response.response.Texto
					});

					alertPopup.then(function(res) {
						$state.go('app.home');
					});

					/** Se guarda el token en el localStorage */
					$localstorage.set(LS.TOKEN, response.response.Token);

					/** Se activa la sesion localStorage */
					$localstorage.set(LS.LOGIN, true);
				}
		}).error(function(response){
			console.log("Error login usuario status: "+response.status);
		});
	};

	/** Funcion para que un usuario recupere la clave */
	$scope.olvidoClave = function () {

		var confirmPopup = $ionicPopup.confirm({
     title: 'Digite su correo para realizar el proceso de recuperación de clave',
     template: '<input type="text" ng-model="$parent.dataCorreoUser">'
   });

   confirmPopup.then(function(res) {
     if(res) {
       console.log('Correo '+$scope.dataCorreoUser);
			 $ionicLoading.show({
				 template: '<ion-spinner icon="lines" class="spinner-calm"></ion-spinner></br>Solicitando Cambio de Clave ... '
			 });
			 data= {
				 Correo: $scope.dataCorreoUser,
			 }
			 usuarioService.consultasUsuario(API.URL_OLVIDO_CLAVE , data)
			 .success(function(response){
				 console.log("Success al solicitar cambio de clave "+response.status);
				 console.log("Success al solicitar cambio de clave "+JSON.stringify(response));
				 $ionicLoading.hide();
				 var alertPopup = $ionicPopup.alert({
					 title:     MENSAJES_ERROR.TITULO_OK,
					 template:  response.response.Texto
				 });
			 }).error(function(response){
				 console.log("Error al solicitar cambio de clave "+reponse.status);
			 });
     } else {
       console.log('You are not sure');
     }
   });
	};
});

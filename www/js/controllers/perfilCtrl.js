angular.module('proyect.controllers.perfilCtrl', [])

/** Controlador para el perfil */
.controller('perfilCtrl',  function(
	LS,
	$scope,
	$ionicHistory,
	$ionicPopup,
	$ionicHistory,
	$ionicLoading,
	$localstorage ,
	$state,
	MENSAJES_ERROR,
	usuarioService,
	API){

	$scope.usuario = {
			UserId:'',
			Nombre:"",
			SegundoNombre:"",
			Apellido:"",
			SegundoApellido:"",
			Correo:"administrativo@administrativo.com",
			DocIdentidad:0,
			NivelEducativo:0,
			IdMunicipio:0,
			Direccion:"",
			Genero:"",
			NumeroCelular:"",
			IdActividadAgricola:0,
			LugarDeProduccion:0,
			IdCategoriaProducto:0,
			PerteneceAsociacion:false,
			Asociacion:"",
			NitAsociacion:"",
			MunicipioName:"",
			LugarDeProduccionName:"",
			Token:null
	}
	$ionicLoading.show({
      template: '<ion-spinner icon="lines" class="spinner-calm"></ion-spinner></br>Cargando Informaci&oacute;n... '
    });
	var user = { Token: $localstorage.get(LS.TOKEN) }
	usuarioService.consultasUsuario(API.URL_DATA_USER ,user )
	.success(function(response){
		$ionicLoading.hide();
		console.log("Success perfilCtrl " + JSON.stringify(	response.response));
		$scope.usuario.UserId = response.response.Data.UserId;
		$scope.usuario.Nombre= response.response.Data.Nombre + " " +response.response.Data.SegundoNombre + " " + response.response.Data.Apellido + " " + response.response.Data.SegundoApellido;
		$scope.usuario.Correo = response.response.Data.Correo;
		$scope.usuario.DocIdentidad=response.response.Data.DocIdentidad;
		$scope.usuario.Direcciony= response.response.Data.Direccion;
		$scope.usuario.Genero= response.response.Data.Genero;
		$scope.usuario.NumeroCelular= response.response.Data.NumeroCelular;

	}).error(function(response){
		console.log("Error perfilCtrl " + JSON.stringify(response.status));
	});


	$scope.guardarInformacion = function(){
		 var dataEnvio = {
		 		Aplicacion: "agroClima",
				UserId  : $localstorage.get(LS.TOKEN),
				Nombre : $scope.usuario.Nombre.split(" ")[0] || '',
				SegundoNombre  : $scope.usuario.Nombre.split(" ")[1] || '',
				Apellido  : $scope.usuario.Nombre.split(" ")[2] || '',
				SegundoApellido : $scope.usuario.Nombre.split(" ")[3] || '',
				Correo : $scope.usuario.Correo,
				DocIdentidad  : $scope.usuario.DocIdentidad,
				//NivelEducativo  : '',
				//IdMunicipio : '',
				Direccion : $scope.usuario.Direcciony,
				Genero : $scope.usuario.Genero,
				NumeroCelular : $scope.usuario.NumeroCelular
				//IdActividadAgricola  : 0,
				//LugarDeProduccion : 0,
				//IdCategoriaProducto  : 0,
				//PerteneceAsociacion  : 0,
				//Asociacion : 0,
				//NitAsociacion  : 0,
				//Superficie : '',
       			//ProduccionPrincipal  : '',
        		//MunicipioName : '',
        		//LugarDeProduccionName  : '',
		 };
		 console.log("Dta de envio "+ JSON.stringify(dataEnvio));
		 usuarioService.consultasUsuario(API.URL_SAVE_DATA_USER , dataEnvio )
		 .success(function(response){
			 console.log("Response success "+JSON.stringify(response));
			 var alertPopup = $ionicPopup.alert({
					title:     MENSAJES_ERROR.TITULO_OK,
					template:  response.response.Texto
			 });

			 alertPopup.then(function(res) {
				//  $scope.iniciarSesion();
				$ionicHistory.nextViewOptions({
				  disableAnimate: true,
				  disableBack: true
				});
				 $state.go('app.home');
			 });
		 }).error(function(response){
			 console.log("Response error "+JSON.stringify(response));
		 });


	};
});

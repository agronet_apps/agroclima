/******************************************************************************
 *                                                                            *
 *     Este fichero se optimiza para evitar la redundancia de codigo en la    *
 *     carga inicial de la aplicacion                                         *
 *                                                                            *
 ******************************************************************************/
angular.module('proyect.controllers.inicioCtrl', [ ])

.controller('inicioCtrl',  function($scope,$ionicHistory , $window, $timeout , $ionicModal , $ionicLoading, MENSAJES_ERROR, MENSAJES_INFORMATIVOS, $ionicPopup, usuarioService, API ,LS, FlightDataService  , $ionicSlideBoxDelegate){

  /**********************************
   *       Buscando municipios
   **********************************/

   var interfaz = {
      municipios : [],
      ciudadActual: '',
      pronostico :'',
      planesRiego : [],
      anosBusqueda : [],
      fechaActual : 'Año',
      fechaActual_2 : 'Año',
      mostrarInformacionPerfil : false,
      seleccionadoFecha : 'Mes',
      seleccionadoFecha_2 : 'Mes',
      seleccionadoCultivo : 'Cultivo',
      actualPronostico : '',
      dataInsertar : {
        municipio : '',
        fecha : '',
        cultivo : ''
      }
   };
   $scope.horario = " en la mañana: ";
   interfaz.pronostico = {
      fecha : formatearFecha('2016-01-01T00000'),
      temperaturaMaxima : '10',
      temperaturaMinina : '5',
      dia : {
        madrugada : 'Pleamares',
        maniana :'Pleamares',
        tarde : 'Pleamares',
        noche :'Pleamares'
      }
   };
   interfaz.tiempo = encontrartiempo(interfaz.pronostico.dia.maniana);
   $scope.interfaz = interfaz;
   $ionicLoading.show({
	      template: '<ion-spinner icon="lines" class="spinner-calm"></ion-spinner></br> '+MENSAJES_INFORMATIVOS.BUSCANDO_MUNICIPIOS
   });
   $scope.mostrarSlide = 'false';
   usuarioService.consultasInformacion(API.URL_MUNICIPIOS , '')
   .success(function(respuesta){

     console.log("Success en el servicio de municipios status:"+JSON.stringify(respuesta));

    // console.log("Success en el servicio de municipios Data:"+JSON.stringify(respuesta.response.Data));
     var data = respuesta.response.Data;

     //var data = respuesta.response;
     for( var i=0 ; i < data.length ; i++){
       interfaz.municipios[i] = {id:data[i].MunicipioId,
                                 nombre:data[i].Nombre +'  ,  '+data[i].Departamento,
                                 key:data[i].ID,
                                codigo : data[i].CodCiudadPronostico
                               };
     }
     localStorage.setItem('MUNICIPIOS' , JSON.stringify(interfaz.municipios));
     response = '';
     /**********************************
      *       Buscando Posicion
      **********************************/


        console.log("Success en el geolocation de consulta de posicion");
        // var localizacionUsuario = {
        //     Long:localStorage.getItem('LON'),
        //     Lat:localStorage.getItem('LAT'),
        // };
        // /**********************************
        //  *       Buscando informacion de la ciudad
        //  **********************************/
        // usuarioService.consultasUsuario( API.URL_ESTACION, localizacionUsuario, '')
        // .success(function(response){
        //     console.log("Success en el servicio de consulta de estacion status:"+response.status);
        //     console.log("Success en el servicio de consulta de estacion status:"+JSON.stringify(response.response));
        //     var dataResponse = response.response.Data;
        //     if(response.response.Estado === -1){
        //     interfaz.ciudadActual = {
        //             nombre : 'Bogota',
        //             id : '11100000',
        //             codigo : 11100000,
        //             key: 11100000
        //           };
        //   }else{
        //         interfaz.ciudadActual = {
        //                 nombre : dataResponse.Nombre,
        //                 id : dataResponse.MunicipioId,
        //                 codigo :  dataResponse.CodCiudadPronostico,
        //                 key: dataResponse.Id
        //               };
        //       }
        interfaz.ciudadActual = JSON.parse(localStorage.getItem('estacion'));
            localStorage.setItem('ACTUAL_CITY',JSON.stringify(interfaz.ciudadActual));
            $ionicLoading.hide();
            $ionicLoading.show({
              template: '<ion-spinner icon="lines" class="spinner-calm"></ion-spinner></br> Buscando Estación'
            });
            /**********************************
             *       Buscando pronostico de la ciudad
             **********************************/
             var pronosticoCiudad = {
               IdCiudad:(interfaz.ciudadActual.codigo),
             }
             usuarioService.consultasUsuario(API.URL_PRONOSTICO , pronosticoCiudad)
             .success(function(response){
               console.log("Success en el servicio de consultando el pronostico status:"+response.status);
               console.log("Success en el servicio de consultando el pronostico JSON:"+JSON.stringify(response.response));
               var dataPronostico = response.response.Data;
               $ionicLoading.hide();
               if(response.response.Estado===0){
                  alert("No fue posible obtener el pronostico del clima");
               }
               else if(response.response.Estado===1){

               
           
                 interfaz.pronostico = {
                    fecha : formatearFecha(dataPronostico.Fecha || '01-01-2016T00000'),
                    temperaturaMaxima : dataPronostico.Tmax || '10',
                    temperaturaMinina : dataPronostico.Tmin || '5',
                    dia : {
                      madrugada :dataPronostico.Madrugada  || 'Pleamares',
                      maniana :dataPronostico.Maniana  || 'Pleamares',
                      tarde :dataPronostico.Tarde  || 'Pleamares',
                      noche :dataPronostico.Noche  || 'Pleamares'
                    }
                 };
              interfaz.pronostico.actualPronostico = dataPronostico.Maniana;
              interfaz.tiempo = encontrartiempo(interfaz.pronostico.dia.maniana);
              }
              else{
                alert("Ocurrio un error en el servidor");
              }
               var token = localStorage.getItem(LS.TOKEN) || '';

               if(token !== ''){
                 $ionicLoading.hide();
                 $ionicLoading.show({
                   template: '<ion-spinner icon="lines" class="spinner-calm"></ion-spinner></br> '+MENSAJES_INFORMATIVOS.BUSCANDO_INFORMACION_USER
                 });
                 /**********************************
                  *       Buscando planes de Riego del usuario
                  **********************************/
                 ConsultaPlanRiego = { Token: token };
                 //ConsultaPlanRiego = { Token: "NTZkMzRmY2RlOTYzODMxOTEyZjAxMmMzMjYwMDE5ZmZiOTVjODdiMzEwZWM4MzE0Mjc5ZGVlNWI3NDhkYzQwYzNlOTYxOTRjM2E2NjUxOTY3NGYyNzc2NTU0YzU4YzBiYWMxYmVkYTg0ODJlNjUxMg==" }
                 usuarioService.consultasUsuario(API.URL_PLAN_RIEGO , ConsultaPlanRiego)
                 .success(function(response){
                   console.log("Success en el servicio de consultando los planes de riego status:"+response.status);
                   //{"IdPlanRiego":5,"NombreCultivo":"CAÑA","Municipio":"2001","FechaInicio":"2016-01"
                   var planesRiego = [];
                   var vectorRiego = [];

                    interfaz.planesRiego = planesRiego;
// TEMPORAL PARA EL FUNCIONAMIENTO DE LA APLICACION INICIO
                   vectorRiego =  response.response.Data;
                   if (response.response.Estado===0){
                      //alert(response.response.Texto);
                   }
                   else if(response.response.Estado===1){
              
                       for(var i = 0 ; i < vectorRiego.length ; i ++){
                         var planRiegoV = {
                            IdPlanRiego : '',
                            NombreCultivo : '',
                            Municipio : '' ,
                            FechaInicio : '',
                            etapas :[],
                            grafica1 : '',
                            grafica2 : '',
                            grafica3 : '',
                            grafica4 : ''
                          }
                         planRiegoV.IdPlanRiego = vectorRiego[i].IdPlanRiego || '';
                         planRiegoV.NombreCultivo = vectorRiego[i].NombreCultivo || '';
                         planRiegoV.Municipio  =  vectorRiego[i].Municipio || '';
                         planRiegoV.FechaInicio = vectorRiego[i].FechaInicio || '';
                         planesRiego[i] = planRiegoV;
                      }
                  }
                  else{
                    alert("Ocurrio un error en la conexión con el servidor");
                  }

// TEMPORAL PARA EL FUNCIONAMIENTO DE LA APLICACION FIN
                   interfaz.mostrarInformacionPerfil = true;
                   /**********************************
                    *       Consultar detalle
                    **********************************/
                    var i = 0 ;
                    // for(; i < planesRiego.length ; i++){
                    function planRiego (i){
                      if(planesRiego[i] !== undefined){
                        var ConsultaPlanRiego = { id: planesRiego[i].IdPlanRiego };
                        console.log(" ########################################### Plan a consultar:"+JSON.stringify(ConsultaPlanRiego));
                        usuarioService.consultasUsuario(API.URL_DETALLE_PLAN , ConsultaPlanRiego)
                        .success(function(response){
                          console.log("Success en el servicio de consulta el detalle de plan status:"+response.status);
                          console.log("Success en el servicio de consulta el detalle de plan status:"+JSON.stringify(response.response.Data));
                          if(response.response.Data !== null && response.response.Data !== undefined){

                          var etapas = response.response.Data.cultivo.Etapas;
                          //console.log("####################################### "+response.response.Data.municipio.Nombre);
                          planesRiego[i].Municipio = response.response.Data.municipio.Nombre;

                          console.log("Success data: "+JSON.stringify(etapas));

  /** Ajuste mockup a las graficas**/
  $scope.series = [etapas[0].NombreEtapa, etapas[1].NombreEtapa, etapas[2].NombreEtapa , etapas[3].NombreEtapa];
  //** Objeto de Slide 1
	  var dato1 = [];
	  var mes = [];
	  for(var k = 0 ; k < etapas[0].mesriego.length ; k++){
		  // dato1[k] = (etapas[0].mesriego[k].Riego * 100);
      dato1[k] = etapas[0].mesriego[k].Riego;
		  mes[k] = buscarMes(etapas[0].mesriego[k].Mes);
	  }

  planesRiego[i].grafica1 = {
       etapaCrecimiento : etapas[0].Etapa.NombreEtapa,
       tiempoCrecimiento : etapas[0].Dias + ' dias',
       laminaCrecimiento : etapas[0].reqHidricos +' mm',
       dataGrafica :  [dato1],
       series : mes,
       color : [{fillColor: '#f09e29', strokeColor: '#f09e29', highlightFill: '#f09e29', highlightStroke: '#f09e29'},]
     };
   //** Objeto de Slide 2
  	   dato1 = [];
	   mes = [];
	   for(var k = 0 ; k < etapas[1].mesriego.length ; k++){
		  //  dato1[k] = (etapas[1].mesriego[k].Riego  * 100);
       dato1[k] = etapas[1].mesriego[k].Riego ;
	 	 mes[k]  = buscarMes(etapas[1].mesriego[k].Mes);
	   }
   planesRiego[i].grafica2 = {
        etapaCrecimiento : etapas[1].Etapa.NombreEtapa,
        tiempoCrecimiento : etapas[1].Dias + ' dias',
        laminaCrecimiento : etapas[1].reqHidricos +' mm',
        dataGrafica :  [dato1,],
        series : mes,
        color : [{fillColor: '#57acef', strokeColor: '#57acef', highlightFill: '#57acef', highlightStroke: '#57acef'},]
      };
    //** Objeto de Slide 3
   	dato1 = [];
    mes = [];
    for(var k = 0 ; k < etapas[2].mesriego.length ; k++){
    	// dato1[k] = (etapas[2].mesriego[k].Riego * 100);
      dato1[k] = etapas[2].mesriego[k].Riego;
  	mes[k] = buscarMes(etapas[2].mesriego[k].Mes);
    }
    planesRiego[i].grafica3 = {
         etapaCrecimiento : etapas[2].Etapa.NombreEtapa,
         tiempoCrecimiento : etapas[2].Dias + ' dias',
         laminaCrecimiento : etapas[2].reqHidricos +' mm',
         dataGrafica :  [dato1,],
         series : mes,
         color : [{fillColor: '#6ab13e', strokeColor: '#6ab13e', highlightFill: '#6ab13e', highlightStroke: '#6ab13e'},]
       };

     //** Objeto de Slide
     var dato1 = [];
     var mes = [];
     for(var k = 0 ; k < etapas[3].mesriego.length ; k++){
    	//  dato1[k] = (etapas[3].mesriego[k].Riego * 100);
       dato1[k] = etapas[3].mesriego[k].Riego ;
   	mes[k]  = buscarMes(etapas[3].mesriego[k].Mes);
     }
     planesRiego[i].grafica4 = {
          etapaCrecimiento : etapas[3].Etapa.NombreEtapa,
          tiempoCrecimiento : etapas[3].Dias + ' dias',
          laminaCrecimiento : etapas[3].reqHidricos +' mm',
          dataGrafica :  [dato1,],
          series : mes,
          color : [{fillColor: '#dd1841', strokeColor: '#dd1841', highlightFill: '#dd1841', highlightStroke: '#dd1841'},]
        };
                          }
     					if(i < planesRiego.length ){
     						i = i + 1 ;
     						planRiego (i);
     					}else{

                 $scope.$apply(function() {
                    $ionicLoading.hide();
                 });
     					}


  /** Ajuste mockup a las graficas */
                        }).error(function(response){
                          console.log("Error en el servicio de consulta el detalle de plan status:"+response.status);
                        });
                      }else{
                        $ionicLoading.hide();
                      }
                    };
                    planRiego(i);
                 }).error(function(response){
                  console.log("Error en el servicio de consultando los planes de riego status:"+response.status);
                 });
               }
            //  }).error(function(response){
            //    console.log("Error en el servicio de consultando el pronostico status:"+response.status);
            //  });

        }).error(function(response){
            console.log("Error en el servicio de consulta de posicion status:"+response.status);
        });

   }).error(function(response){
     console.log("Error en el servicio de municipios status:"+response.status);
   });
   /**********************************
    *       Buscando planes de Riego del usuario   interfaz.pronostico.dia.maniana
    **********************************/
   $scope.estadoClima = function(clima){
    if(clima === 1){
      $scope.interfaz.pronostico.actualPronostico =  interfaz.pronostico.dia.maniana;
      $scope.horario = " en la mañana: ";
    }else if( clima === 2){
      $scope.interfaz.pronostico.actualPronostico = interfaz.pronostico.dia.tarde;
      $scope.horario = " en la tarde: ";
    }else if( clima === 3){
      $scope.interfaz.pronostico.actualPronostico = interfaz.pronostico.dia.noche;
      $scope.horario = " en la noche  : ";
    }
    interfaz.tiempo = encontrartiempo($scope.interfaz.pronostico.actualPronostico);
  };
  /**********************************
  *       Template modal de filtro de las ciudades
  **********************************/
    $ionicModal.fromTemplateUrl('templates/directives/modalMunicipios.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal = modal;
    });
   $scope.openModal = function() {
    $scope.modal.show();
    };
    $scope.closeModal = function() {
      $scope.modal.hide();
    };
    $scope.seleccionado = function(municipio){
      var pronosticoCiudad = {
        IdCiudad:(municipio.codigo),
      }
      $scope.search = municipio.nombre;
      $scope.interfaz.ciudadActual.nombre = municipio.nombre;
      console.log("municipio "+JSON.stringify(pronosticoCiudad));
      $ionicLoading.show({
        template: '<ion-spinner icon="lines" class="spinner-calm"></ion-spinner></br> '+MENSAJES_INFORMATIVOS.BUSCANDO_PRONOSTICO
      });
      console.log("Data "+JSON.stringify(pronosticoCiudad));
      usuarioService.consultasUsuario(API.URL_PRONOSTICO , pronosticoCiudad)
      .success(function(response){
        $ionicLoading.hide();
        console.log("Success en el servicio de consultando el pronostico status:"+response.status);
        var dataPronostico = response.response.Data;
        $ionicLoading.hide();
        if(dataPronostico !== null){
          console.log("Data Server "+JSON.stringify(dataPronostico));
          interfaz.pronostico = {
             fecha : formatearFecha(dataPronostico.Fecha),
             temperaturaMaxima : dataPronostico.Tmax,
             temperaturaMinina : dataPronostico.Tmin,
             dia : {
               madrugada :dataPronostico.Madrugada,
               maniana :dataPronostico.Maniana,
               tarde :dataPronostico.Tarde,
               noche :dataPronostico.Noche
             }
          };
          interfaz.pronostico.actualPronostico = dataPronostico.Maniana;
          console.log("Data Capturada "+JSON.stringify(interfaz.pronostico));
          interfaz.tiempo = encontrartiempo(interfaz.pronostico.dia.maniana);
        }else{
          var alertPopup = $ionicPopup.alert({
						 title:     MENSAJES_ERROR.TITULO_OK,
						 template:  "No se encuentra informaci&oacute;n sobre esta ubicaci&oacute;n"
					});
        }
        $scope.modal.hide();
      }).error(function(){
        console.log("Error en el servicio de consultando el pronostico status:"+response.status);
      });
    };

    $scope.getWindowOrientation = function () {
        return $window.orientation;
      };

      $scope.$watch($scope.getWindowOrientation, function (newValue, oldValue) {
        $scope.degrees = newValue;
      }, true);

      angular.element($window).bind('orientationchange', function () {
        $scope.$apply();
      });



      $scope.descargarBoletin = function() {
        //var fechaActual = new Date();

        //var mes = buscarMes(fechaActual.getMonth() -1);
        //var ano = fechaActual.getFullYear();

        //  $ionicLoading.show({
        //     template: '<ion-spinner icon="lines" class="spinner-calm"></ion-spinner></br>Cargando informaci&oacute;n... '
        //   });
         data = {Agno :$scope.interfaz.fechaActual ,
           Mes :  $scope.interfaz.seleccionadoFecha
        };

        console.log(JSON.stringify(data));
         usuarioService.consultasUsuario(API.URL_BOLETIN , data)
         .success(function(response){
           $ionicLoading.hide();
           console.log("Success en consultar boletin Response "+JSON.stringify(response));

           console.log(response);
           console.log(response.response.Data.Href);
           if(response.response.Data.Href != null){

             $scope.boletin = {linkBoletin : response.response.Data.Href ,
                                info : response.response.Data.Text
             };
              $scope.descargar($scope.boletin.linkBoletin);
           }

         }).error(function(response){
           console.log("Error en consultar boletin Response "+JSON.stringify(response));
         });

      };

      $scope.mostrarTooltip = function (){
        var alertPopup = $ionicPopup.alert({
           title: 'Informaci&oacute;n!',
           template: 'Aquí se presenta los requerimientos hídricos netos en mm por mes, en función de los elementos del clima de una estación y ubicación en particular. Esta es la cantidad de agua que requiere la planta a nivel mensual de acuerdo con su etapa fenológica y a su evapotranspiración de referencia calculada con los registros medios históricos, pero cada agricultor debe tener en cuenta que debe aplicar una mayor cantidad de agua en función de la eficiencia de su sistema de riego y de las características de sus suelos. Entre menos eficiente y tecnificado sea su sistema de riego debe aplicar una mayor cantidad de agua. Esta es una herramienta que apoyará la planeación de su cultivo. Todas las salidas se presentan en mm ya que a 1 mm equivale a 10m3/ha o a 1 lt/m2.'
         });
      };
      $ionicModal.fromTemplateUrl('templates/directives/modalCultivos.html', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function(modal) {
        $scope.modalCultivos = modal;
      });
      $ionicModal.fromTemplateUrl('templates/directives/modalmeses.html', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function(modal) {
        $scope.modalmeses = modal;
      });
      $ionicModal.fromTemplateUrl('templates/directives/modalmeses_2.html', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function(modal) {
        $scope.modalmeses2 = modal;
      });
      $scope.openModalCultivos = function() {
        $ionicLoading.show({
           template: '<ion-spinner icon="lines" class="spinner-calm"></ion-spinner></br>Cargando informaci&oacute;n... '
         });
         //Consultar categoria cultivo
         $scope.selectCategorias = [ ];

         usuarioService.consultasInformacion(API.URL_CATEGORIAS_CULTIVOS , '')
         .success(function(response){
           $ionicLoading.hide();
           console.log("Success en consultar categoria cultivos Response "+JSON.stringify(response));
          //  console.log("");
          var respuesta = response.response.Data;
            for(var i = 0 ;i < respuesta.length ; i++){
              $scope.selectCategorias[i] ={
                      Id : respuesta[i].IdCultivo,
                      CategoriaCultivo:respuesta[i].NombreCultivo
                    }
            }
         }).error(function(response){
           console.log("Error en consultar categoria cultivos Response "+JSON.stringify(response));
         });
       console.log("AbriendoModal");
      $scope.modalCultivos.show();
      };
      $scope.closeModalCultivos = function() {
        $scope.modalCultivos.hide();
      };
      $scope.seleccionadoCultivos= function (cultivo){
        $scope.interfaz.seleccionadoCultivo = cultivo.CategoriaCultivo;
        console.log("Municipio "+JSON.stringify(cultivo));
        $scope.interfaz.dataInsertar.cultivo = cultivo.Id;
        $scope.modalCultivos.hide();
      };

      $scope.openModalMeses = function (mes){
          $scope.modalmeses.show();
      };
      $scope.openModalMeses2 = function (mes){
          $scope.modalmeses2.show();
      };
      $scope.mesSeleccionado = function (mes){
        $scope.interfaz.seleccionadoFecha = buscarMes(mes);
        $scope.modalmeses.hide();
      };
      $scope.mesSeleccionado2 = function(mes){
        $scope.interfaz.seleccionadoFecha_2 = buscarMes(mes);
        $scope.modalmeses2.hide();
      }
      $scope.buscarRecomendacion = function (){
        $ionicLoading.show({
           template: '<ion-spinner icon="lines" class="spinner-calm"></ion-spinner></br>Cargando informaci&oacute;n... '
         });
         data ={Agno : $scope.interfaz.fechaActual_2,
           Mes :  $scope.interfaz.seleccionadoFecha_2,
          cultivo : $scope.interfaz.seleccionadoCultivo, //'Arroz'//
        };
      // data = { Agno:"2016",  Mes:"Febrero",  cultivo:"Arroz"};
        console.log(JSON.stringify(data));
         usuarioService.consultasUsuario(API.URL_RECOMENDACION , data)
         .success(function(response){
           $ionicLoading.hide();
           console.log("Success en consultar categoria cultivos Response "+JSON.stringify(response));
          $scope.informacion = [];
          var respuesta = response.response.Data;
            if(respuesta !== null){
              for(var i = 0 ;i < respuesta.length ; i++){
                $scope.informacion[i] ={
                        region : respuesta[i].region,
                        descripcion:respuesta[i].descripcion
                      }
              }
            }else{
              var alertPopup = $ionicPopup.alert({
                 title: 'Informaci&oacute;n!',
                 template: 'No se encontro recomendaci&oacute;n'
               });
              alertPopup.then(function(){
                $scope.modal3.hide();
              });
            }
         }).error(function(response){
           console.log("Error en consultar categoria cultivos Response "+JSON.stringify(response));
         });
       console.log("AbriendoModal");
      };

      $scope.toggleGroup = function(group) {
  if ($scope.isGroupShown(group)) {
    $scope.shownGroup = null;
  } else {
    $scope.shownGroup = group;
  }
};
$scope.isGroupShown = function(group) {
  return $scope.shownGroup === group;
};

$scope.descargar = function(url){
//   console.log("Direccion a descargar: "+ url);
//   $cordovaFileOpener2.open(
//     // '/sdcard/Download/starwars.pdf',
// url,
//     'application/pdf'
//   ).then(function() {
//       // file opened successfully
//       console.log("Abieto el pdf");
//   }, function(err) {
//       // An error occurred. Show a message to the user
//       console.log("Error el pdf");
//   });
switch(device.platform){
		case 'Android':

			/**
			 * Android devices cannot open up PDFs in a sub web view (inAppBrowser) so the PDF needs to be downloaded and then opened with whatever
			 * native PDF viewer is installed on the app.
			 */

       $ionicLoading.show({
          template: '<ion-spinner icon="lines" class="spinner-calm"></ion-spinner></br>Descargando Boletin... '
        });
			var fileURL = cordova.file.externalDataDirectory+"local.pdf";
      console.log("RUta donde se guardara el fichero "+fileURL);
			var fileTransfer = new FileTransfer();
			var uri = encodeURI( url );

			fileTransfer.download(
				uri,
				fileURL,
				function(entry) {
					//$scope.data.localFileUri = entry.toURL();
          $ionicLoading.hide();
					// window.plugins.fileOpener.open(entry.toURL());
          cordova.plugins.fileOpener2.open(
              // '/sdcard/Download/starwars.pdf', // You can also use a Cordova-style file uri: cdvfile://localhost/persistent/Download/starwars.pdf
              entry.toURL(),
              'application/pdf',
              {
                  error : function(e) {
                      console.log('Error status: ' + e.status + ' - Error message: ' + e.message);
                  },
                  success : function () {
                      console.log('file opened successfully');
                  }
              }
          );
          $scope.modalBoletin.hide();
				},
				function(error) {
          $scope.modalBoletin.hide();
          $ionicLoading.hide();
          $ionicPopup.alert({
						 title:     MENSAJES_ERROR.TITULO_OK,
						 template:  "No se encontro el boletin disponible" });
				},
				false
			);


			break;
		default:

			/**
			 * IOS and browser apps are able to open a PDF in a new sub web view window. This uses the inAppBrowser plugin
			 */
			var ref = window.open(url, '_blank', 'location=no,toolbar=yes,closebuttoncaption=Close PDF,enableViewportScale=yes');
			break;
	}
};

  $scope.closeModalRecomendaciones = function(){
    $scope.modal3.hide();
  };
  $scope.openModalAnos = function(){

    var yearNow = new Date().getYear();

    if(yearNow < 2000 ){
      yearNow = yearNow  + 1900 ;
    }
    $scope.interfaz.fechaActual  = 'Año';
    // $scope.interfaz.fechaActual_2  = 'Año';
    $scope.interfaz.anosBusqueda [0] = {valor: yearNow};
    $scope.interfaz.anosBusqueda [1] = {valor: (yearNow - 1)};
    $ionicModal.fromTemplateUrl('templates/directives/modalAnos.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modalAnos = modal;
      $scope.interfaz.fechaActual  = 'Año';
      // $scope.interfaz.fechaActual_2  = 'Año';
      $scope.modalAnos.show();
    });
  };
  $scope.openModalAnos_2= function(){

  var yearNow = new Date().getYear();

  if(yearNow < 2000 ){
    yearNow = yearNow  + 1900 ;
  }
  $scope.interfaz.anosBusqueda [0] = {valor: yearNow};
  $scope.interfaz.anosBusqueda [1] = {valor: (yearNow - 1)};
  $ionicModal.fromTemplateUrl('templates/directives/modalAnos_2.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modalAnos_2 = modal;
    $scope.modalAnos_2.show();
  });
};
  $scope.anoSeleccionado = function (ano){
      $scope.interfaz.fechaActual = ano.valor;
      $scope.modalAnos.hide();
  };
  $scope.anoSeleccionado_2 = function (ano){
      $scope.interfaz.fechaActual_2  = ano.valor;
      $scope.modalAnos_2.hide();
  };

  $scope.closeModalAnos = function(){
      $scope.modalAnos.hide();
  };


/** Menu modal**/
$scope.abrirMenu =  function(){
  $ionicModal.fromTemplateUrl('templates/directives/modalMenuBoletin.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modalMenuBoletin = modal;
    $scope.modalMenuBoletin.show();
  });
}
$scope.closeModalMenuBoletin = function() {
  $scope.modalMenuBoletin.hide();
};

$ionicModal.fromTemplateUrl('templates/directives/modalRecomendaciones.html', {
  scope: $scope,
  animation: 'slide-in-up'
}).then(function(modal) {
  $scope.interfaz.fechaActual  = 'Año';
$scope.interfaz.seleccionadoFecha = 'Mes';
  $scope.modal3 = modal;
});

$scope.closeModalRecomendaciones = function() {
  $scope.modal3.hide();
};

$scope.mostrarRecomendaciones = function(){
  $scope.closeModalMenuBoletin();
  $scope.modal3.show();
};

$ionicModal.fromTemplateUrl('templates/directives/modalBoletin.html', {
  scope: $scope,
  animation: 'slide-in-up'
}).then(function(modal) {
  $scope.interfaz.fechaActual  = 'Año';
  $scope.interfaz.seleccionadoFecha = 'Mes';
  $scope.modalBoletin = modal;
});

$scope.closeModalBoletin = function() {
  $scope.modalBoletin.hide();
};

$scope.mostrarBoletin = function(){
  $scope.closeModalMenuBoletin();

  $scope.modalBoletin.show();

};
});

/** Se utiliza para saber que template utilizar**/
var encontrartiempo = function (tiempoServer){
    switch (tiempoServer) {
      case 'Creciente Súbita':
          return 'lluvioso';
      break;
      case 'Inundación':
          return 'lluvioso';
      break;
      case 'Deslizamiento':
          return 'nublado';
      break;
      case 'Incendios':
          return 'nublado';
      break;
      case 'Contaminación':
          return 'nublado';
      break;
      case 'Heladas':
          return 'nublado';
      break;
      case 'Mar de Leva':
          return 'nublado';
      break;
      case 'Huracán':
          return 'nublado';
      break;
      case 'Tormenta Tropical':
          return 'lluvioso';
      break;
      case 'Oleaje Alto':
          return 'lluvioso';
      break;
      case 'Inundacion Lenta':
          return 'lluvioso';
      break;
      case 'Viento Fuerte y Oleaje en Ascenso':
          return 'lluvioso';
      break;
      case 'Onda Tropical':
          return 'nublado';
      break;
      case 'Altas Temperaturas':
          return 'nublado';
      break;
      case 'Lluvias Ligeras':
          return 'lluvioso';
      break;
      case 'Soleado':
          return 'nublado';
      break;
      case 'Noche Despejada':
          return 'nublado';
      break;
      case 'Cielo Ligeramente Nublado':
          return 'nublado';
      break;
      case 'Cielo Ligeramente Nublado':
          return 'nublado';
      break;
      case 'Cielo Parcialmente Nublado':
          return 'nublado';
      break;
      case 'Cielo Parcialmente Nublado':
          return 'nublado';
      break;
      case 'Nublado':
          return 'nublado';
      break;
      case 'Lluvias':
          return 'lluvioso';
      break;
      case 'Lloviznas':
          return 'lluvioso';
      break;
      case 'Lluvias y Tormentas Electricas':
          return 'lluvioso';
      break;
      case 'Lloviznas en Diferentes Sectores':
          return 'lluvioso';
      break;
      case 'Lluvias Moderadas':
          return 'lluvioso';
      break;
      case 'Lluvias Fuertes':
          return 'lluvioso';
      break;
      case 'Lluvias en Diferentes Sectores':
          return 'lluvioso';
      break;
      case 'Vientos Fuertes':
          return 'nublado';
      break;
      case 'Depresion Tropical':
          return 'nublado';
      break;
      case 'Incremento del Oleaje':
          return 'nublado';
      break;
      case 'Cielo Despejado':
          return 'soleado';
      break;
      case 'Despejado':
          return 'soleado';
      break;
      case 'Cielo Despejado':
          return 'soleado';
      break;
      case 'Oleaje y Viento':
          return 'nublado';
      break;
      case 'Pleamares':
          return 'soleado';
      break;
      case 'Tiempo Lluvioso':
          return 'lluvioso';
      break;
    }
    // if( === ){
    //   return "nublado";
    // }else if(tiempoServer === ){
    //   return "nublado";
    // }else if(tiempoServer === "Lloviznas"){
    //   return "lluvioso"
    // }else if(tiempoServer === "Cielo Parcialmente Nublado"){
    //   return "nublado";
    // }
};
/** Se utiliza para cambiar el formato de la fecha**/
var formatearFecha = function(fecha){
  //2016-02-15T00:00:00"
  var fechaForm = fecha.split("T")[0];
  var retorno  = fechaForm.split("-")[2] + " de "+ buscarMes(fechaForm.split("-")[1]) + " del "+fechaForm.split("-")[0];
  return retorno;
};
/** Se utiliza para saber el mes actual**/
var buscarMes = function (mesNumerico){
  var retorno;
  switch(parseInt(mesNumerico)){
    case 1:
            retorno = "Enero";
            break;
    case 2:
            retorno = "Febrero";
            break;
    case 3:
            retorno = "Marzo";
            break;
    case 4:
            retorno = "Abril";
            break;
    case 5:
            retorno = "Mayo";
            break;
    case 6:
            retorno = "Junio";
            break;
    case 7:
            retorno = "Julio";
            break;
    case 8:
            retorno = "Agosto";
            break;
    case 9:
            retorno = "Septiembre";
            break;
    case 10:
            retorno = "Octubre";
            break;
    case 11:
            retorno = "Noviembre";
            break;
    case 12:
            retorno = "Diciembre";
            break;
  };
  return retorno;
}


var constantes = angular.module("proyect.constantes", []);
/** Constantes API */
//ENDPOINT PUBLICA

//var ENDPOINT = "http://186.155.199.197:8000";

// var ENDPOINT = "http://186.155.199.197:8000";
var ENDPOINT = "http://serviciosmadr.minagricultura.gov.co/saviawebapi";
//var ENDPOINT="http://104.196.130.110/saviaWebApi";

//ENDPOINT INTERNA
//var ENDPOINT = "http://192.168.1.134:8081";
constantes.constant("API", {
    "URL_REGISTRO_USUARIO"    : ENDPOINT+"/api/agro/Registro",
    "URL_INICIAR_SESION"      : ENDPOINT+"/api/agro/IniciarSesion",
    "URL_OLVIDO_CLAVE"        : ENDPOINT+"/api/agro/RecoveryPass",
    "URL_CAMBIO_CLAVE"        : ENDPOINT+"/api/agro/CambiarContrasena",
    "URL_CATEGORIAS_CULTIVOS" : ENDPOINT+"/api/AgroClima/GetAllCultivos",
    "URL_PRONOSTICO"          : ENDPOINT+"/api/AgroClima/getPronosticoClima",
    "URL_ESTACION"            : ENDPOINT+"/api/Agro/GetLocation",
    "URL_PLAN_RIEGO"          : ENDPOINT+"/api/AgroClima/getPlanesRiego",
    "URL_MUNICIPIOS"          : ENDPOINT+"/api/Agro/ObtenerTodosMunicipios",
    "URL_CREAR_PLAN"          : ENDPOINT+"/api/AgroClima/CrearPlanRiego",
    "URL_DETALLE_PLAN"        : ENDPOINT+"/api/AgroClima/getPlanRiego",

    "URL_DATA_USER"			  : ENDPOINT+"/api/agro/ObtenerPerfil",
    "URL_SAVE_DATA_USER"	  : ENDPOINT+"/api/agro/ActualizarPerfil",

    "URL_RECOMENDACION"       : ENDPOINT+"/api/AgroClima/GetRecomandacionClima",
    "URL_CLIMA"               : ENDPOINT+"/api/AgroClima/getClimaMensual",
    "URL_CLIMA_HISTORICO"     : ENDPOINT+"/api/AgroClima/getClimaMensualMB1900",
    "URL_CLIMA_ACTUAL"        : ENDPOINT+"/api/AgroClima/getClimaMensualMB",
    "URL_BOLETIN"             : ENDPOINT+"/api/AgroClima/getBoletinAgroClimatico",
    "METHOD"                  : "POST",
    "HEADERS"                 : {
                              	'Access-Control-Allow-Origin' : '*',
                              	'Access-Control-Allow-Methods' : 'POST, GET, OPTIONS, PUT',
                              	'Content-Type': 'application/json',
                              	'Accept': 'application/json'
                              },
    "TIME_OUT"                 : 70000
});

/** Constantes Mensajes error */
constantes.constant("MENSAJES_ERROR", {
    "TITULO_ERROR": "Ups!",
    "TITULO_OK": "Correcto",
    "CONEXION_TIME_OUT": "El servidor no responde intente mas tarde!",
    "ACTIVACION_GPS" : "Por favor active el GPS o verifique su conexi&oacute;n",
    "BUSCAR_CIUDAD" : "Se encontr&oacute; un error al buscar la ciudad",
    "BUSCAR_ESTACION": "Se encontr&oacute; un error al buscar la estaci&oacute;n",
});
/** Constantes Mensajes error */
constantes.constant("MENSAJES_INFORMATIVOS", {
    "CARGANDO": "Cargando ...",
    "BUSCANDO_DIRECCION": "Buscando Direcci&oacute;n...",
    "BUSCANDO_POSICION": "Buscando Posici&oacute;n...",
    "BUSCANDO_PRONOSTICO": "Buscando pronostico...",
    "BUSCANDO_ESTACION": "Buscando estaci&oacute;n...",
    "BUSCANDO_INFORMACION_USER" : "Buscando informaci&oacute;n Adicional ...",
    "BUSCANDO_MUNICIPIOS":"Buscando municipios ...",
});
/** Constantes localStorage */
constantes.constant("LS", {
    "TOKEN": "token",
    "LOGIN": "login"
});

/** Constantes genericas **/
constantes.constant("GET_URL", {
    "CATEGORIA": "1",
    "PRONOSTICO": "2"
});

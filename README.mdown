# AgroClima
<br />
AgroClima es la herramienta del Ministerio de Agricultura y
Desarrollo  Rural,  que  permite  consultar  recomendaciones
agroclimáticas, el pronóstico del tiempo y reportes de clima
por estación. Brinda información para crear el plan de riego
aproximado. Cuenta con el apoyo del IDEAM y CIAT.
<br />



## Plugins
<br />
Agregar los siguientes plugins<br />
cordova plugin add  cordova-plugin-console<br />
cordova plugin add  cordova-plugin-device<br />
cordova plugin add  cordova-plugin-dialogs<br />
cordova plugin add  cordova-plugin-splashscreen<br />
cordova plugin add  cordova-plugin-whitelist<br />
cordova plugin add  cordova-plugin-x-socialsharing<br />
cordova plugin add  ionic-plugin-keyboard<br />


## Plataforma
<br />
Plataformas soportadas android, ios wp8<br />

cordova platform add android<br />
cordova platform add ios<br />
cordova platform add wp8<br />